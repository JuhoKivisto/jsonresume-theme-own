var fs = require("fs");
var path = require('path');
var Handlebars = require("handlebars");
var serializer = require("xmlserializer");
const { template } = require("handlebars");


var emptyDot = fs.readFileSync("svgs/emptyDot.svg", "utf-8");
var filledDot = fs.readFileSync("svgs/filledDot.svg", "utf-8");
//var emptyDotStr = serializer.serializeToString(emptyDot);



function render(resume) {

	console.log(emptyDot);

	resume.skills.forEach(function (skill){
		//var skillLevelStr = skill.level.toString();
		//skillLevelStr = skillLevelStr.replaceAll("○", emptyDot);
		//console.log(skillLevelStr);
		//skill.level = JSON.parse(skillLevelStr);
	});

	var css = fs.readFileSync(__dirname + "/style.css", "utf-8");
	var tpl = fs.readFileSync(__dirname + "/resume.hbs", "utf-8");
	var partialsDir = path.join(__dirname, 'partials');
	var filenames = fs.readdirSync(partialsDir);
	
	filenames.forEach(function (filename) {
		var matches = /^([^.]+).hbs$/.exec(filename);
		if (!matches) {
			return;
		}
		var name = matches[1];
		var filepath = path.join(partialsDir, filename)
		var template = fs.readFileSync(filepath, 'utf8');
		
		Handlebars.registerPartial(name, template);
	});
	
	//tpl.replaceAll("O", "Ö");
	
	var t = "hghg";
	
	t = t.replaceAll("h", "v");
	console.log(t);
	
	//var resumeStr = resume.toString();

	//resume = resumeStr.replaceAll("○", emptyDot);

	var compiledResume = Handlebars.compile(tpl)({
		css: css,
		resume: resume
	});

	compiledResume = compiledResume.replaceAll("○", emptyDot);
	compiledResume = compiledResume.replaceAll("∙", filledDot);
		
	console.log("test")
	return compiledResume;
}

module.exports = {
	render: render
};